# Recsys Twitter Challenge SS21

Our model is an ensemble model, consisting of multiple stages:

### 00. Preprocessing of the data
is conducted in 00_Preprocessing.ipynb - we create a training, test and validation set for further use.

### 01. Run various models on the training data
all models with the prefix "01_" use the input data, train the model on the training data and predict on the test data. Outputs are saved "meta_input". 

### 02. merge data of all models to create input file for meta model
all files saved in "meta_input" are merged into one file in "02_merge_input_for_meta.ipynb".

### 03. run the meta model
the input file from 3. is used to train the meta model. 


### 04. apply performance metrics for model output
the output of our final model is evaluated using 05_metrics_per_submodel.ipynb
