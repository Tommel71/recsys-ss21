
import numpy as np
import pandas as pd
from executables_katrin.hashtag_executable import get_user_hashtag_similarity_from_series
from executables_katrin.MF_model import *
from executables_katrin.Userprofile_executable import *
from executables_katrin.Content_Based_TFIDF_predict import *
from executables_katrin.CBA import *
from executables_katrin.UU_model import *
from executables_katrin.II_model import *
from executables_katrin.metamodel import *
'''
You need to structure your implementation in a way that in this class the prediction
of the different engagement types of your model are called. Please replace the 
random return placeholder with you actual implementation.

You can train different models for each engagement type or you train one which is able
to predicte multiple classes.
'''

current_tweet = "None"
prediction = None

def predict_full(input_line):
    # add hashtag output
    input_metamodel = get_user_hashtag_similarity_from_series(input_line)
    
    # add UU CF
    input_metamodel = input_metamodel.append(predict_UU(input_line))
    
    # add II CF
    input_metamodel = input_metamodel.append(predict_II(input_line))

    # add doc2vec
    input_metamodel = input_metamodel.append(get_similarity_as_series(input_line))
    
    # add userprofiles(non-text)
    up = get_userprofile_similarity_from_series(input_line)
    input_metamodel = input_metamodel.append(up)
    
    # add Matrix Factorization
    mfm = MFModel()
    mf_output = mfm.predict(input_line)
    mf_output = mf_output.rename(lambda x: "MF_"+x)
    input_metamodel = input_metamodel.append(mf_output)
    
    # add TF-IDF
    res = predict_single_sample(input_line)
    tfidf = res[["tfidf_reply","tfidf_retweet","tfidf_retweet_comment","tfidf_like"]]
    input_metamodel = input_metamodel.append(tfidf)
    
    return input_metamodel

def reply_pred_model(input_features):
    global current_tweet
    global prediction
    if current_tweet != input_features.tweet_id:
        prediction = predict_full(input_features)
        current_tweet = input_features.tweet_id
    return predict_reply(prediction)[0]

def retweet_pred_model(input_features):
    global current_tweet
    global prediction
    if current_tweet != input_features.tweet_id:
        prediction = predict_full(input_features)
        current_tweet = input_features.tweet_id
    return predict_retweet(prediction)[0]

def quote_pred_model(input_features):    
    global current_tweet    
    global prediction
    if current_tweet != input_features.tweet_id:
        prediction = predict_full(input_features)
        current_tweet = input_features.tweet_id
    return predict_retweet_comment(prediction)[0]

def fav_pred_model(input_features):
    global current_tweet
    global prediction    
    if current_tweet != input_features.tweet_id:
        prediction = predict_full(input_features)
        current_tweet = input_features.tweet_id
    return predict_like(prediction)[0]