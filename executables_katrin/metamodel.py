#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pickle
import pandas as pd
import numpy as np
import sklearn.preprocessing as pp
from scipy import sparse as sp
from scipy.sparse.linalg import norm


# In[19]:


with open("tpot_results/like_model.pkl","rb") as model:
    like_model = pickle.load(model)
    
with open("tpot_results/reply_model.pkl","rb") as model:
    reply_model = pickle.load(model)
    
with open("tpot_results/retweet_model.pkl","rb") as model:
    retweet_model = pickle.load(model)
    
with open("tpot_results/retweet_comment_model.pkl","rb") as model:
    retweet_comment_model = pickle.load(model)
    
with open("tpot_results/imputer.pkl","rb") as file:
    imputer = pickle.load(file)


# In[20]:


def encode(x):
    engagement_types = ["like","reply","retweet","retweet_comment"]
    abbreviation_list = ["HT","CB_DV","Userprofile", "UU", "II", "MF", "tfidf"]

    # input dictionary
    dataframe_dictionary = {}
    for e_type in engagement_types:
        column_list = [e_type]
        for abb in abbreviation_list:
            column_list.append(abb+"_"+e_type)
        dataframe_dictionary[e_type]=x[column_list]    

    test_dict = {}
    # Here we can build in a for-loop for the engagement types
    for e_type in engagement_types:
        type_data=dataframe_dictionary[e_type]
        test_dict[e_type]={}
        X_test = type_data[type_data.index[1:]]
        y_test = type_data[type_data.index[0]]
        test_dict[e_type]["X_test"] = X_test
        test_dict[e_type]["y_test"] = y_test        
    return test_dict


# In[23]:


def predict_like(x):
    test_dict = encode(x)
    testing_features = imputer.transform(np.array(test_dict["like"]["X_test"]).reshape(1, -1))
    results = like_model.predict(testing_features)
    return results

def predict_reply(x):
    test_dict = encode(x)
    testing_features = imputer.transform(np.array(test_dict["like"]["X_test"]).reshape(1, -1))
    results = reply_model.predict(testing_features)
    return results

def predict_retweet(x):
    test_dict = encode(x)
    testing_features = imputer.transform(np.array(test_dict["like"]["X_test"]).reshape(1, -1))
    results = retweet_model.predict(testing_features)
    return results

def predict_retweet_comment(x):
    test_dict = encode(x)
    testing_features = imputer.transform(np.array(test_dict["like"]["X_test"]).reshape(1, -1))
    results = retweet_comment_model.predict(testing_features)
    return results


# In[22]:




