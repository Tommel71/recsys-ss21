#!/usr/bin/env python
# coding: utf-8

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"></ul></div>

# In[1]:


import pandas as pd
import numpy as np


# In[2]:


def preprocess(path):
    df = pd.read_csv(path, delimiter = '\x01', header = None)
    df.columns = ["text_tokens", "hashtags", "tweet_id", "media", "links", "domains", "type", "language", "timestamp",
              "user_id_engaged", "follower_count_engaged", "following_count_engaged", "is_verified_engaged", "account_creation_time_engaged",
              "user_id_engaging", "follower_count_engaging", "following_count_engaging", "is_verified_engaging", "account_creation_time_engaging",
              "engagee_follows_engager", "reply_engagement_timestamp", "retweet_engagement_timestamp", "retweet_with_comment_engagement_timestamp", "like_engagement_timestamp"]

    single_val = ["tweet_id", "type", "language", "timestamp",
                  "user_id_engaged", "follower_count_engaged", "following_count_engaged", "is_verified_engaged", "account_creation_time_engaged",
                  "user_id_engaging", "follower_count_engaging", "following_count_engaging", "is_verified_engaging", "account_creation_time_engaging",
                  "engagee_follows_engager", "reply_engagement_timestamp", "retweet_engagement_timestamp", "retweet_with_comment_engagement_timestamp", "like_engagement_timestamp"]

    for c in df.columns:
        if c not in single_val:
            df[c] = df[c].apply(lambda x: [] if str(x) == "nan" else str(x).split("\t"))
    
#     print(df.head())
#     print(df.timestamp.min())
#     print(df.timestamp.max())
    
    return df


# # In[3]:


# df = preprocess("~/shared/data/project/training/one_hour")


# # In[18]:


# test = df.loc[df.timestamp >df.timestamp.max()-300]
# train = df.loc[df.timestamp <= df.timestamp.max()-300]


# # In[19]:


# train.to_pickle("train.pkl")
# test.to_pickle("test.pkl")


# # In[7]:


# df.to_pickle("one_hour_preprocessed.pkl")


# # In[6]:


# df_val = preprocess("~/shared/data/project/validation/one_hour")
# df_val.to_pickle("val.pkl")


# # In[4]:


# df_loaded = pd.read_pickle('one_hour_preprocessed.pkl')
# df_loaded.head()


# # In[8]:


# test = df_loaded.hashtags.astype(str)


# # In[12]:




