import pickle
import pandas as pd
import numpy as np
import sklearn.preprocessing as pp
from scipy import sparse as sp
from scipy.sparse.linalg import norm
     
def predict_UU(line):
    
    sample_input = line.copy()
    
    engagements = preprocess(sample_input)

    u_id = engagements["user_id"].iloc[-1]
    i_id = engagements["tweet_id"].iloc[-1]

    predictions = engagements.iloc[-1]

    cols = ["reply", "retweet", "retweet_comment", "like"]

    for col in cols:

        ratings = engagements[["user_id", "tweet_id", col]]
        ratings.rename(columns={"tweet_id":"item", col:"rating"}, inplace=True)

        # cast 1 to the rating 5 and 0 to 1
        one_mask = ratings["rating"] == 1
        ratings["rating"][one_mask] = 5
        ratings["rating"][~one_mask] = 1

        R = sp.csr_matrix((ratings.rating.astype("int64"), (ratings.user_id.astype("int64"), ratings.item.astype("int64"))))
        R_dok = R.todok()

        user_sums = R.sum(axis=1).A1 ## matrix converted to 1-D array via .A1
        user_cnts = (R != 0).sum(axis=1).A1
        user_avgs = user_sums / user_cnts

        prediction = predict_rating(u_id, i_id, R)

        predictions["UU_"+col] = prediction

        
    predictions["user_id"] = sample_input["user_id_engaging"]
    predictions["tweet_id"] = sample_input["tweet_id"]
    
    predictions = predictions.drop(labels=['user_id', 'tweet_id', "reply", "retweet", "retweet_comment", "like"])
        
    return pd.Series(predictions)
    
    
def preprocess(target):
    line = target.copy()
    df = pd.read_pickle('train.pkl')
    df = df.append(line)
    
    userIds = df.user_id_engaging.unique()
    userIds.sort()
    m = userIds.size

    tweetIds = df.tweet_id.unique()
    tweetIds.sort()
    n = tweetIds.size

    numEngagements = len(df)

    #print ("There are", m, "users,", n, "tweets and", numEngagements, "recorded engagements.")

    ## create internal ids for movies and users, that have consecutive indexes starting from 0
    tweetId_to_tweetIDX = dict(zip(tweetIds, range(0, tweetIds.size)))
    tweetIDX_to_tweetId = dict(zip(range(0, tweetIds.size), tweetIds))

    userId_to_userIDX = dict(zip(userIds, range(0, userIds.size )))
    userIDX_to_userId = dict(zip(range(0, userIds.size), userIds))

    engagements = pd.concat([df['user_id_engaging'].map(userId_to_userIDX), df['tweet_id'].map(tweetId_to_tweetIDX), df['reply_engagement_timestamp'],df['retweet_engagement_timestamp'],
           df['retweet_with_comment_engagement_timestamp'],df['like_engagement_timestamp']], axis=1)
    
    engagements.columns = ['user_id', 'tweet_id' ,'reply', 'retweet', 'retweet_comment', 'like']
    
    columns = engagements[['reply', 'retweet', 'retweet_comment', 'like']]

    for col in columns:
        engagements.loc[~engagements[col].isna(), col] = 1
        engagements.loc[engagements[col].isna(), col] = 0
        
    return engagements


def compute_pairwise_user_similarity(u_id, v_id, R):
    
    R = R.copy()
    u = R[u_id,:].copy()
    v = R[v_id,:].copy()
    cent_u = u.copy()
    cent_v = v.copy()
    cent_u.data = u.data - user_avgs[u_id]
    cent_v.data = v.data - user_avgs[v_id]

    numerator = (cent_u*cent_v.T)[0,0]
    denominator = sp.linalg.norm(cent_u)* sp.linalg.norm(cent_v)

    if denominator == 0:
        similarity = 0.
    else:
        similarity = numerator/denominator

    return similarity


def compute_user_similarities(u_id, R):

    R_copy = R.copy()
    user_sums = R.sum(axis=1).A1 ## matrix converted to 1-D array via .A1
    user_cnts = (R != 0).sum(axis=1).A1
    user_avgs = user_sums / user_cnts
    d = sp.diags(user_avgs, 0)
    dummy = R_copy.copy()
    dummy.data = np.ones_like(dummy.data)
    R_copy = R_copy - d*dummy
    R_copy = pp.normalize(R_copy, norm='l2', axis=1)
    u = R_copy[u_id,:].copy()
    uU = R_copy.dot(u.T)

    return uU.toarray().flatten()


def create_user_neighborhood(u_id, i_id, R):
    k = 5
    R = R.copy()
    R_dok = R.todok()
    m = m = R.shape[0]
    
    uU = compute_user_similarities(u_id, R)
    uU_copy = uU.copy() ## so that we can modify it, but also keep the original
    users = np.array([i for i in range(m)])
    # should not contain itself
    uU_copy = np.delete(uU_copy, [u_id])
    users = np.delete(users, [u_id])
    # remove users that didnt have the item
    has_item_mask = [(i, i_id) in R_dok for i in range(m) if i!=u_id]
    uU_copy = uU_copy[has_item_mask]
    users = users[has_item_mask]
    # abs
    data_temp = uU_copy.copy()

    # get the inds for the top k
    inds = np.argsort(data_temp)[::-1][:k]

    nh = {users[i]:uU_copy[i] for i in inds}

    return nh


def predict_rating(user_id, tweet_id, R):
    
    R = R.copy()
    u_id = user_id
    i_id = tweet_id
    user_sums = R.sum(axis=1).A1 ## matrix converted to 1-D array via .A1
    user_cnts = (R != 0).sum(axis=1).A1
    user_avgs = user_sums / user_cnts

    nh = create_user_neighborhood(u_id, i_id, R)
    
    neighborhood_weighted_avg = np.array([nh[v] * (R[v,i_id]-R[v,:].data.mean()) for v in nh.keys()]).sum() / \
                        (np.absolute(np.asarray(list(nh.values()))).sum()+0.1)
    prediction = user_avgs[u_id] + neighborhood_weighted_avg

    
    return prediction