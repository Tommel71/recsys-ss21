#!/usr/bin/env python
# coding: utf-8

# # Tweet-Tweet similarity

# ## Preparatory work

# In[1]:


import pandas as pd

get_ipython().system('pip install gensim')

from sklearn.base import BaseEstimator
from gensim.models import Doc2Vec
from gensim.models.doc2vec import TaggedDocument


# Build class to construct Doc2Vec model...


class Doc2Vectorizer(BaseEstimator):
    def __init__(self, window_size=7, vector_size=300):
        self.window = window_size
        self.vector_size = vector_size
        
    def fit(self, X, y):
        docs = [TaggedDocument(X[i], [str(i)]) for i in range(len(X))]
        self.doc_vec = Doc2Vec(docs, 
                               vector_size = self.vector_size,
                               window = self.window,
                               min_count = 1,
                               workers = 4)
        return self
    
    def transform(self, X):
        return [self.doc_vec.infer_vector(X[i]) for i in range(len(X))]


# Build Doc2Vec model for each subset of the whole dataset belonging to a language...

# Persist Doc2Vec models...

import glob

doc2vec_models = dict()
doc2vec_transforms = dict()

def reload_map():
    for idx, pickle in enumerate(glob.glob('doc2Vec/doc2Vec_*.pkl')):
        language = pickle.replace('.pkl', '').split('_')[-1]
        #tweet_data_lang = tweet_data[tweet_data["language"] == language].reset_index()
        doc2vec_models[language] = Doc2Vectorizer()
        doc2vec_models[language].doc_vec = Doc2Vec.load(pickle)
        #doc2vec_transforms[language] = [doc2vec_models[language].infer_vector(tweet_data_lang.loc[i, "text_tokens"]) 
                                        #for i in tweet_data_lang.index]

# Find most similar documents... (might use FAISS here as well)
# 
# See also: https://towardsdatascience.com/detecting-document-similarity-with-doc2vec-f8289a9a7db7



def find_most_similar(target_tweet, topn=50):
    target_doc = doc2vec_models[target_tweet.language].transform(X=[target_tweet.text_tokens])
    return doc2vec_models[target_tweet.language].doc_vec.dv.most_similar(positive=target_doc, 
                                                                         negative=[], 
                                                                         topn=topn)

def find_least_similar(target_tweet, topn=50):
    target_doc = doc2vec_models[target_tweet.language].transform(X=[target_tweet.text_tokens])
    return doc2vec_models[target_tweet.language].doc_vec.dv.most_similar(positive=[], 
                                                                         negative=target_doc, 
                                                                         topn=topn)

def find_most_similar_embed(lang, target_tweet_embed, topn=50):
    return doc2vec_models[lang].doc_vec.dv.most_similar(positive=target_tweet_embed, 
                                                                         negative=[], 
                                                                         topn=topn)

def find_least_similar_embed(lang, target_tweet_embed, topn=50):
    return doc2vec_models[lang].doc_vec.dv.most_similar(positive=[], 
                                                                         negative=target_tweet_embed, 
                                                                         topn=topn)


# ### User-Tweet similarity

# In[ ]:


from scipy import spatial
import numpy as np

def popularity_preds(lang, new_embed):
    most_popular = find_most_similar_embed(lang=lang, target_tweet_embed=new_embed, topn=12)
    least_popular = find_least_similar_embed(lang=lang, target_tweet_embed=new_embed, topn=12)
    sum_like = 0
    sum_reply = 0
    sum_retweet = 0
    sum_retweet_comment = 0
    sums = 0
    
    for prev_id, similarity in most_popular:
        tweet_row = train_data[train_data["prev_index"] == int(prev_id)].iloc[0, :]
        sums = sums + 1
        sum_like = sum_like + tweet_row["like_engagement_timestamp"] * similarity
        sum_reply = sum_reply + tweet_row["like_engagement_timestamp"] * similarity
        sum_retweet = sum_retweet + tweet_row["retweet_engagement_timestamp"] * similarity
        sum_retweet_comment = sum_retweet_comment + tweet_row["retweet_with_comment_engagement_timestamp"] * similarity
    
    for prev_id, similarity in least_popular:
        tweet_row = train_data[train_data["prev_index"] == int(prev_id)].iloc[0, :]
        sums = sums + 1
        sum_like = sum_like - tweet_row["like_engagement_timestamp"] * similarity
        sum_reply = sum_reply - tweet_row["like_engagement_timestamp"] * similarity
        sum_retweet = sum_retweet - tweet_row["retweet_engagement_timestamp"] * similarity
        sum_retweet_comment = sum_retweet_comment - tweet_row["retweet_with_comment_engagement_timestamp"] * similarity
        
    return sum_retweet / sums, sum_reply / sums, sum_like / sums, sum_retweet_comment / sums

def get_user_tweet_similarity(new_tweet):
    try:
        new_embed = doc2vec_models[new_tweet.language].transform(X=[new_tweet.text_tokens])
    except KeyError:
        print('Haven''t encountered this language in training: ' + new_tweet.language)
        return 0, 0, 0, 0
    try:
        user_engaging_data = train_data.loc[(new_tweet.user_id_engaging, new_tweet.language)]
    except KeyError:
        return popularity_preds(new_tweet.language, new_embed)
    
    user_tweets_reply_yes = user_engaging_data[user_engaging_data["reply_engagement_timestamp"] == 1]
    
    user_tweets_reply_yes_embed = doc2vec_models[new_tweet.language].transform(X=user_tweets_reply_yes.text_tokens.tolist())

    # compare averaged embedding with embedding of new tweet (cosine similarity)
    reply_pred = 1 - spatial.distance.cosine(new_embed, np.mean(user_tweets_reply_yes_embed, axis=0))
    
    user_tweets_retweet_yes = user_engaging_data[user_engaging_data["retweet_engagement_timestamp"] == 1]
    user_tweets_retweet_yes_embed = doc2vec_models[new_tweet.language].transform(X=user_tweets_retweet_yes.text_tokens.tolist())

    # compare averaged embedding with embedding of new tweet (cosine similarity)
    retweet_pred = 1 - spatial.distance.cosine(new_embed, np.mean(user_tweets_retweet_yes_embed, axis=0))
    
    user_tweets_retweet_comment_yes = user_engaging_data[user_engaging_data["retweet_with_comment_engagement_timestamp"] == 1]
    user_tweets_retweet_comment_yes_embed = doc2vec_models[new_tweet.language].transform(X=user_tweets_retweet_comment_yes.text_tokens.tolist())

    # compare averaged embedding with embedding of new tweet (cosine similarity)
    retweet_comment_pred = 1 - spatial.distance.cosine(new_embed, np.mean(user_tweets_retweet_comment_yes_embed, axis=0))
    
    user_tweets_like_yes = user_engaging_data[user_engaging_data["like_engagement_timestamp"] == 1]
    user_tweets_like_yes_embed = doc2vec_models[new_tweet.language].transform(X=user_tweets_like_yes.text_tokens.tolist())

    # compare averaged embedding with embedding of new tweet (cosine similarity)
    like_pred = 1 - spatial.distance.cosine(new_embed, np.mean(user_tweets_like_yes_embed, axis=0))
    
    return retweet_pred, reply_pred, like_pred, retweet_comment_pred


def get_similarity_as_series(tweet):
    retweet, reply, like,retweet_comment = get_user_tweet_similarity(tweet)
    predictions = {'CB_DV_retweet' : retweet, 'CB_DV_reply' : reply, 'CB_DV_like' : like, 'CB_DV_retweet_comment' : retweet_comment}
    return pd.Series(predictions)

# ## Summed up in API

# Running this from the main notebook...


def content_based_docvec(path_train):
    train_data = pd.read_pickle(path_train)
    
    target_variables = ["reply_engagement_timestamp", 
                        "retweet_engagement_timestamp", 
                        "retweet_with_comment_engagement_timestamp", 
                        "like_engagement_timestamp"]

    for tgt_var in target_variables:
        train_data.loc[~train_data[tgt_var].isna(), tgt_var] = 1
        train_data.loc[train_data[tgt_var].isna(), tgt_var] = 0
        
    languages_sorted = train_data.groupby("language").count().sort_values("text_tokens", ascending=False).text_tokens
    languages = languages_sorted.index
    
    doc2vec_models = dict()
    doc2vec_transforms = dict()

    reload_map()
    
    train_data['prev_index'] = train_data.index
    train_data.sort_values(by='user_id_engaging', inplace=True)
    train_data = train_data.set_index(['user_id_engaging', 'language'])
    
    return train_data

train_data = content_based_docvec("train.pkl")
