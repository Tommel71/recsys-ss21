import pandas as pd
import numpy as np 
import matplotlib as plt
import seaborn as sns
from scipy import sparse as sp
from scipy.sparse.linalg import norm
import sklearn.preprocessing as pp
import pickle
from numpy import linalg as la


def calculate_userprofile_similarity(tweet_vector, userprofile_all, engaging_type):
    userprofile = userprofile_all[engaging_type]
    
    numerator = np.dot(tweet_vector,userprofile)
    denominator = la.norm(tweet_vector)*la.norm(userprofile)
    if denominator == 0:
        similarity = 0
    else:
        similarity = numerator / denominator
    
    return similarity

def get_userprofile_similarity_from_series(line):
    series = line.copy()
    
    # Werte aus Trainingsdatensatz
    fr_training_mean_value = 0.6304988643286538
    fr_training_range = 266.0
    
    engaging_types = ['reply', 'retweet', 'retweet_comment', 'like']
    user_profiles = pd.read_csv('ines_intermediates/user_profiles.csv')
    test_data = series
    test_data.drop(["hashtags","language", 'timestamp', 'user_id_engaged',"text_tokens",
           'is_verified_engaged', 'account_creation_time_engaged',
           'follower_count_engaging',
           'following_count_engaging', 'is_verified_engaging',
           'account_creation_time_engaging'], inplace=True) #removed axis
    test_data = test_data[['tweet_id','user_id_engaging',"media",'links','domains','type',
                           'follower_count_engaged','following_count_engaged','engagee_follows_engager',
           'reply_engagement_timestamp', 'retweet_engagement_timestamp',
           'retweet_with_comment_engagement_timestamp',
           'like_engagement_timestamp']]
    test_data=test_data.rename({"user_id_engaging":"user_id",'follower_count_engaged':"follower",'following_count_engaged':"following", },axis="index")
    columnnames_engaging_types = ['Userprofile_reply', 'Userprofile_retweet', 'Userprofile_retweet_comment', 'Userprofile_like']
    engaging_column_names = ['reply_engagement_timestamp','retweet_engagement_timestamp','retweet_with_comment_engagement_timestamp','like_engagement_timestamp']
    
    for tgt_var in engaging_column_names:
        if np.isnan(test_data[tgt_var]):
            test_data[tgt_var] = 0.0
        else:
            test_data[tgt_var] = 1.0
    rename_dict = dict(zip(engaging_column_names,engaging_types))
    test_data=test_data.rename(rename_dict, axis=1)
    
    for i in range(len(engaging_types)):
        test_data[columnnames_engaging_types[i]] = np.nan
        
    # Media
    test_data["Photo"] = 1.0 if "Photo" in test_data['media'] else -1.0
    test_data["Video"] = 1.0 if "Video" in test_data['media'] else -1.0
    test_data["GIF"] = 1.0 if "GIF" in test_data['media'] else -1.0
    
    # Type
    test_data["TopLevel_type"] = 1.0 if "TopLevel" in test_data['type'] else -1.0
    test_data["Quote_type"] = 1.0 if "Quote" in test_data['type'] else -1.0
    test_data['Retweet_type'] = 1.0 if "Retweet" in test_data['type'] else -1.0
    
    # Link
    test_data["links_domain"] = 1.0 if len(test_data["links"]) != 0 or len(test_data["domains"]) != 0 else -1.0
    
    # follow rate
    test_data['follow_rate'] = test_data.following/max(test_data.follower,1)
    test_data.follow_rate = max(min((test_data.follow_rate-fr_training_mean_value)/fr_training_range,1.0),-1.0)
    
    #engagee follows engager
    test_data['engagee_follows_engager']=test_data['engagee_follows_engager'].astype(int)
    test_data['engagee_follows_engager_new'] = -1.0 if test_data['engagee_follows_engager']==0 else 1.0
    
    user_id_available = test_data["user_id"] in user_profiles.index
 
    for idx in range(len(columnnames_engaging_types)):
        cname = columnnames_engaging_types[idx]
        engaging_type = engaging_types[idx]
        
        if not user_id_available:
            similarity = 0.0
        else:
            userprofile_all = user_profiles[test_data["user_id"]]
            tweet_vector = np.array(test_data[["Photo","Video","GIF","TopLevel_type","Quote_type",'Retweet_type',"links_domain",'follow_rate','engagee_follows_engager_new']],dtype=float)
            similarity = calculate_userprofile_similarity(tweet_vector, userprofile_all, engaging_type)
            
        test_data[cname] = similarity
    
    test_data = test_data[['Userprofile_reply','Userprofile_retweet','Userprofile_retweet_comment','Userprofile_like']]
    
    return test_data

