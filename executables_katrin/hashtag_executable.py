from scipy import spatial
import pickle
import numpy as np
import pandas as pd

def get_user_hashtag_list_similarity(engagement_type, user_id, hashtag_list, user_profiles, hashtag_vectors):
    # engagement types: ['reply', 'retweet', 'ret_com', 'like']
    if user_id not in user_profiles[engagement_type].index:
        return 0.0
    user_profile = np.array(user_profiles[engagement_type][user_id], dtype=float)
    current_hashtag_vectors = []
    for hashtag in hashtag_list:
        if hashtag in hashtag_vectors.index:
            current_hashtag_vectors.append(hashtag_vectors[hashtag])
    if len(current_hashtag_vectors) == 0:
        return 0.0
    else:
        tweet_hashtag_vector = np.mean(current_hashtag_vectors, axis=0)
        result = (1 - spatial.distance.cosine(user_profile,tweet_hashtag_vector))/max(np.linalg.norm(user_profile)/np.linalg.norm(tweet_hashtag_vector),np.linalg.norm(tweet_hashtag_vector)/np.linalg.norm(user_profile))
        return result
    

def get_user_hashtag_similarity_from_series(input_line):
    series = input_line.copy()
    # loading the trained profile vectors
    # maybe you need to adapt the path here @Katrin"
    with open('magnus_intermediates/hashtag_vectors', 'rb') as handle:
        hashtag_vectors  = pickle.load(handle)
    with open('magnus_intermediates/user_profiles', 'rb') as handle:
        user_profiles = pickle.load(handle)
    test_data = series
    test_data.drop(['media', 'links', 'domains',
           'type', 'language', 'timestamp', 'user_id_engaged',"text_tokens",
           'follower_count_engaged', 'following_count_engaged',
           'is_verified_engaged', 'account_creation_time_engaged',
           'follower_count_engaging',
           'following_count_engaging', 'is_verified_engaging',
           'account_creation_time_engaging', 'engagee_follows_engager'], inplace=True) #removed axis
    test_data = test_data[['tweet_id','user_id_engaging','hashtags',
           'reply_engagement_timestamp', 'retweet_engagement_timestamp',
           'retweet_with_comment_engagement_timestamp',
           'like_engagement_timestamp']]
    test_data=test_data.rename({"user_id_engaging":"user_id"},axis="index")
    engaging_types = ['reply', 'retweet', 'retweet_comment', 'like']
    columnnames_engaging_types = ['HT_reply', 'HT_retweet', 'HT_retweet_comment', 'HT_like']
    engaging_column_names = ['reply_engagement_timestamp','retweet_engagement_timestamp','retweet_with_comment_engagement_timestamp','like_engagement_timestamp']

    for tgt_var in engaging_column_names:
        if np.isnan(test_data[tgt_var]):
            test_data[tgt_var] = 0.0
        else:
            test_data[tgt_var] = 1.0
    rename_dict = dict(zip(engaging_column_names,engaging_types))
    
    for i in range(len(engaging_types)):
        test_data[columnnames_engaging_types[i]] = np.nan
    test_data=test_data.rename(rename_dict, axis=1)
    test_data = test_data[['tweet_id','user_id', 'hashtags', 'HT_reply', 'HT_retweet','HT_retweet_comment', 'HT_like', 'reply', 'retweet','retweet_comment', 'like']]
    if test_data["hashtags"]==[]:
        for i in range(len(columnnames_engaging_types)):
            test_data[columnnames_engaging_types[i]] = 0.0
    else:
        for i in range(len(columnnames_engaging_types)):
            result = get_user_hashtag_list_similarity(engagement_type = engaging_types[i], user_id=test_data["user_id"], hashtag_list=test_data["hashtags"], user_profiles = user_profiles, hashtag_vectors = hashtag_vectors)
            test_data[columnnames_engaging_types[i]] = result
    test_data.drop(["hashtags"],inplace=True)
    return test_data
