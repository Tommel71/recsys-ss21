#!/usr/bin/env python
# coding: utf-8

# # TF-IDF predict
# This notebook runs a prediction on either a dataset, or on a single series

# ## Imports

import pandas as pd
import pickle
import random
import glob
import matplotlib.pyplot as plt
from time import time
from transformers import BertModel
from transformers import BertTokenizer

from sklearn.base import BaseEstimator
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import make_scorer
from sklearn.metrics import plot_confusion_matrix
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from transformers import AutoModel
from transformers import AutoTokenizer
import logging
import xml.etree.ElementTree as ET


from pprint import pprint
from collections import Counter
from functools import reduce

from sklearn.metrics.pairwise import cosine_similarity 
from sklearn.metrics.pairwise import linear_kernel
import multiprocessing





def preprocess(df):
    userIds = df.user_id_engaging.unique()
    userIds.sort()
    m = userIds.size

    tweetIds = df.tweet_id.unique()
    tweetIds.sort()
    n = tweetIds.size

    numEngagements = len(df)

    #print ("There are", m, "users,", n, "tweets and", numEngagements, "recorded engagements.")

    engagements = pd.concat([df['user_id_engaging']
                             , df['tweet_id']
                             , df['reply_engagement_timestamp']
                             , df['retweet_engagement_timestamp']
                             , df['retweet_with_comment_engagement_timestamp']
                             , df['like_engagement_timestamp']
                             , df['language']
                             , df['text_tokens']
                            ]
                             
                            , axis=1
                           )
    engagements.columns = ['user_id', 'tweet_id', 'reply', 'retweet', 'retweet_comment', 'like', 'language', 'text_tokens']

    
    
    columns = engagements[['reply', 'retweet', 'retweet_comment', 'like']]
    for col in columns:
        engagements.loc[~engagements[col].isna(), col] = 1
        engagements.loc[engagements[col].isna(), col] = 0

    ## Generate tokenizer
    tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased')

    ## Detokenize
    #### Still need to fix the special tokens - they fudge up the predictions later
    engagements['text'] = engagements.apply(lambda row : tokenizer.decode(row['text_tokens']
                                                                          , add_special_tokens=False)
                                 , axis = 1)

    ## Clean up mess - No longer needed (I think) - Either that, or skip decoding at the first place
    engagements.drop('text_tokens', axis=1, inplace=True)
    engagements.head(5)

    engagements['reply'].value_counts()
    engagements['retweet'].value_counts()
    engagements['retweet_comment'].value_counts()
    
    user_counts = engagements['user_id'].value_counts()
    
    return engagements


# ## Load helpers from training
# These are results from running the training.
# This includes:
# * Dictionary of the features for each language trained
# * A pipeline per language
# * List of languages that are available (if sample is not in pretrained, we take the biggest model)
# * Dictionary of the training data per language.

with open('GK_intermediates/X_train_dict.pkl', 'rb') as handle:
    X_train_dict  = pickle.load(handle)


with open('GK_intermediates/pipeline_dict.pkl', 'rb') as handle:
    pipeline_dict  = pickle.load(handle)

with open('GK_intermediates/train_data_dict.pkl', 'rb') as handle:
    train_data_dict  = pickle.load(handle)

with open('GK_intermediates/train_top_n_languages.pkl', 'rb') as handle:
    train_top_n_languages  = pickle.load(handle)


# ## Define functions for predictions
def similar_documents(text, df, colName, language, n=10):
        # define correct subset
        df2 = pd.DataFrame(train_data_dict[language].copy())
        # transform input
        input_vect = pipeline_dict[language].transform([text])
        # Cosine similarity (linear_kernel is faster)
        df2['similarity'] = linear_kernel(input_vect, X_train_dict[language]).flatten()
        # sort and return subset
        df2 = df2.sort_values(by='similarity', ascending=False)[['similarity', colName]].head(n)
        # negative value for non-engagement
        df2[colName].replace(0,-0.1, inplace=True)
        # Weight score with similarity
        df2[colName] = df2[colName]*df2['similarity']
        return (df2[colName])


# In[10]:
def predict_engagement(text, colName, language, n=10):
    # Find most similar documents
    most_similar_documents = similar_documents(text=text, df=train_data_dict[language],colName=colName, language=language, n=n)
    # and calculate prediction - Should be moved to a regression of sorts
    res = most_similar_documents.mean()
    return res


# In[11]:
def predictSeries(serie, n=10):
    # Define what to predict:
    cols = ["reply", "retweet", "retweet_comment", "like"]
    
    # Read in relevant variables
    sval = serie['text']
    lan = serie['language']
    # Overload to most common language if not found
    if lan in train_top_n_languages:
        lan = lan
    else:
        lan = train_top_n_languages[0]
    
    # Predict
    for col in cols:
        serie['tfidf_' + col] = predict_engagement(sval, col, language=lan, n=n)
    # Cleanse for correct format
    serie.drop(labels=['language', 'text'], inplace=True)
    serie = pd.DataFrame(serie)
    serie = serie.transpose()
    return(serie)


# # Predict for a single sample

# ## Define the function
# Still requires functions and pickles from above

# In[21]:
def predict_single_sample(series):
    input_df = pd.DataFrame(series).transpose()
    input_preprocessed = preprocess(input_df)
    prediction = predictSeries(input_preprocessed.iloc[0], 25)
    prediction = prediction.iloc[0]
    return prediction