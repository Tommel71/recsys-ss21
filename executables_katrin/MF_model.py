
import pickle
import pandas as pd

cols = ["reply", "retweet", "retweet_comment", "like"]

class MFModel:
    
    
    def __init__(self):
        
        self.models = {}
        
        for col in cols:

            model_path = "tom_models/" + col + ".pkl"
            model = pickle.load(open(model_path,'rb'))
            self.models[col] = model
            
        
    def predict(self, line):
        
        line = self.preprocess(line)[["user_id", "item_id"]]
        
        predictions = {}
        
        for col in cols:
            prediction = self.models[col].predict(line)
            predictions[col] = prediction[0]
            
        return pd.Series(predictions)
    
    
    def preprocess(self, df):
        df = pd.DataFrame(df).transpose()
        engagements = df[['user_id_engaging', 'tweet_id', 'reply_engagement_timestamp', 'retweet_engagement_timestamp', 'retweet_with_comment_engagement_timestamp', 'like_engagement_timestamp']]
        engagements.columns = ['user_id', 'tweet_id' ,'reply', 'retweet', 'retweet_comment', 'like']
        columns = engagements[['reply', 'retweet', 'retweet_comment', 'like']]

        for col in columns:
            engagements.loc[~engagements[col].isna(), col] = 1
            engagements.loc[engagements[col].isna(), col] = 0


        return engagements.rename(columns={"tweet_id":"item_id"})
    


#line = pickle.load(open("sample_input_model.pkl" ,'rb'))
#mfm = MFModel()
#mfm.predict(line)